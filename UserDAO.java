
package com.example.ExpenseReimbursementSystem;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO {
    private static JDBC_Connection jdbc_connection;

    public UserDAO()
    {
        jdbc_connection = JDBC_Connection.getInstance();
    }

    /*public void updateUser(User user) throws SQLException {
        String query = "insert into TMUSER(," + "username," + "password) VALUES (?, ?,)";
        PreparedStatement ps = jdbc_connection.conn.prepareStatement(query);
        ps.setString(2, user.getUsername());
        ps.setString(3, user.getPassword());
        ps.executeQuery();
        ps.executeUpdate();
    }*/


    public ArrayList<User> getAllUsers() throws SQLException
    {
        ArrayList<User> users = new ArrayList<>();
        String query = "select * from TMUser";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        while(rs.next())
        {
            User user = new User();
            user.setUserID(rs.getInt("user_ID"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setEmail(rs.getString("email"));
            user.setUserRoleID(rs.getInt("user_role_id"));
            users.add(user);
        }
        return users;
    }

    public ArrayList<User> getSpecificUser(String x) throws SQLException
    {
        ArrayList<User> users = new ArrayList<>();
        String query = "select * from TMUser where USERNAME = x";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        while(rs.next())
        {
            User user = new User();
            user.setUserID(rs.getInt("user_ID"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            users.add(user);
        }
        return users;
    }

    public void viewAllUsers() throws SQLException
    {
        ArrayList<User> viewUsers = getAllUsers();

        for(int i = 0; i < viewUsers.size(); i++)
        {
            System.out.println(viewUsers.get(i));
        }
    }

    public void viewAllEmployees() throws SQLException
    {
        ArrayList<User> viewEmployees = getAllEmployees();

        for(int i = 0; i < viewEmployees.size(); i++)
        {
            System.out.println(viewEmployees.get(i));
        }
    }

    public ArrayList<User> getAllEmployees() throws SQLException
    {
        ArrayList<User> employees = new ArrayList<>();
        String query = "select * from TMUSER INNER JOIN TMUSER_ROLE ON  TMUSER.USER_ROLE_ID = TMUSER_ROLE.USER_ROLE_ID where TMUSER_ROLE.USER_ROLE = 'employee'";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        while(rs.next())
        {
            User user = new User();
            user.setUserID(rs.getInt("user_ID"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("first_Name"));
            user.setLastName(rs.getString("last_Name"));
            user.setEmail(rs.getString("email"));
            user.setUserRoleID(rs.getInt("user_Role_ID"));
            employees.add(user);
        }

        return employees;
    }

    public boolean checkRole(String x) throws SQLException {

        boolean check = false;

        ArrayList<User> viewEmployees = getAllEmployees();

        for(int i = 0; i < viewEmployees.size(); i++)
        {
            if(viewEmployees.get(i).getUsername().equals(x))
            {
                check = true;
            }

        }
        return check;
    }

    public String getRole(String x) throws SQLException {

        String result = x;

        if(checkRole(result))
            result = "employee";
        else
            result= "finance manager";

        return result;
    }

    public boolean verifyUser(String x, String y) throws SQLException
    {
        boolean check = false;

        ArrayList<User> viewUsers = getAllUsers();

        for(int i = 0; i < viewUsers.size(); i++)
        {
            if(viewUsers.get(i).getUsername().equals(x) && viewUsers.get(i).getPassword().equals(y))
            {
                check = true;
            }
        }

        return check;
    }

    public int getUserID(String x) throws SQLException
    {
        int id = 0;

        ArrayList<User> viewUsers = getAllUsers();

        for(int i = 0; i < viewUsers.size(); i++)
        {
            if(viewUsers.get(i).getUsername().equals(x))
            {
                id = viewUsers.get(i).getUserID();
            }
        }

        if(id == 0)
        {
            System.out.println("Something went really wrong");
        }

        return id;
    }
}

