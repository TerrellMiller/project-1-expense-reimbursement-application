package com.example.ExpenseReimbursementSystem;

public class Reimbursement {
    private int reimbID;
    private double amount;
    private String description;
    private String reimType;
    private String reimbStatus;
    private int userID;

    public Reimbursement() {
    }

    public Reimbursement(int reimbID, double amount, String description, String reimType, int userID) {
        this.reimbID = reimbID;
        this.amount = amount;
        this.description = description;
        this.reimType = reimType;
        this.reimbStatus = null;
        this.userID = userID;
    }

    public int getReimbID() {
        return reimbID;
    }

    public void setReimbID(int reimbID) {
        this.reimbID = reimbID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReimType() {
        return reimType;
    }

    public void setReimType(String reimType) {
        this.reimType = reimType;
    }

    public String getReimbStatus() {
        return reimbStatus;
    }

    public void setReimbStatus(String reimbStatus) {
        this.reimbStatus = reimbStatus;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "reimbID=" + reimbID +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", reimType='" + reimType + '\'' +
                ", reimbStatus='" + reimbStatus + '\'' +
                ", userID=" + userID +
                '}';
    }
}


