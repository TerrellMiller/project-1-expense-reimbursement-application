package com.example.ExpenseReimbursementSystem;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


@WebServlet(name = "Login", value = "/Login")
public class Login extends HttpServlet {

    public Login()
    {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
        String jsonString = request.getReader().readLine();
        ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(jsonString, User.class);
        UserDAO userDAO = new UserDAO();

        try {
            if(userDAO.verifyUser(user.getUsername(), user.getPassword()))
            {
                if(userDAO.checkRole(user.getUsername()))
                {
                    request.getSession().setAttribute("usernameID", userDAO.getUserID(user.getUsername()));
                    request.getSession().setAttribute("type", userDAO.getRole(user.getUsername()));
                    System.out.println("MY SESSION ID" + userDAO.getUserID(user.getUsername()));
                    response.sendRedirect("/Expense_Reimbursement_System/employee_dashboard.html");
                }
                else
                {
                    request.getSession().setAttribute("usernameID", userDAO.getUserID(user.getUsername()));
                    System.out.println("MY SESSION ID" + userDAO.getUserID(user.getUsername()));
                    response.sendRedirect("/Expense_Reimbursement_System/finance_manager_dashboard.html");
                }
            }
            else
            {
                response.sendRedirect("/Expense_Reimbursement_System/login.html");
            }
        }
        catch (SQLException throwables) {
            System.out.println("\nSomething went wrong to be in this. Sorry\n");

            throwables.printStackTrace();
        }
    }
}
