package com.example.ExpenseReimbursementSystem;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "SubmitForm", value = "/SubmitForm")
public class SubmitForm extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String jsonString = request.getReader().readLine();
        ObjectMapper objectMapper = new ObjectMapper();
        Reimbursement reimbursement = objectMapper.readValue(jsonString, Reimbursement.class);
        ReimbursementDAO reimbursementDAO= new ReimbursementDAO();
        try {
            reimbursementDAO.updateReimbursement(reimbursement);
        } catch (SQLException throwables) {
            System.out.println("Reimbursement failed to submit");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
