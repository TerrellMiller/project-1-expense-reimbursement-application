package com.example.ExpenseReimbursementSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserRoleDAO {
    private static JDBC_Connection jdbc_connection;

    public UserRoleDAO()
    {
        jdbc_connection = JDBC_Connection.getInstance();
    }

   /* public void updateReimbursement(User user) throws SQLException {
        String query = "insert into (," + "username," + "password) VALUES (?, ?, ?)";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ps.setString(2, user.getUsername());
        ps.setString(3, user.getPassword());
        ps.executeQuery();
        ps.executeUpdate();
    }*/

    public ArrayList<UserRole> getAllUserRoles() throws SQLException
    {
        String query = "select * from TMUSER_ROLE";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<UserRole> userRoles = new ArrayList<>();

        while(rs.next())
        {
            UserRole userRole = new UserRole();
            userRole.setUserRoleID(rs.getInt("USER_ROLE_ID"));
            userRole.setUserRole(rs.getString("USER_ROLE"));
            userRoles.add(userRole);
        }

        return userRoles;
    }

    public String getUserRole(int x) throws SQLException
    {
        String role = " ";
        ArrayList<UserRole> viewRoles = getAllUserRoles();

        for(int i = 0; i < viewRoles.size(); i++)
        {
            if(viewRoles.get(i).getUserRoleID() == x)
            {
                role = viewRoles.get(i).getUserRole();
            }
        }

        if(role.equals(""))
        {
            System.out.println("Something went wrong");
        }

        return role;
    }
}
