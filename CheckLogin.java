package com.example.ExpenseReimbursementSystem;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "CheckLogin", value = "/CheckLogin")
    public class CheckLogin extends HttpServlet {

        public CheckLogin()
        {
            super();
        }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");

        if(request.getSession().getAttribute("usernameID") == null)
        {
            response.getWriter().append("Access Denied");
            request.getSession().invalidate();
        }
        else
        {
            if(request.getSession().getAttribute("type") == "employee")
            {
                response.getWriter().append("Access Granted");
            }

            if(request.getSession().getAttribute("type") == "finance manager")
            {
                response.getWriter().append("Access Granted");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
