package com.example.ExpenseReimbursementSystem;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "ViewAllEmployees", value = "/ViewAllEmployees")
public class ViewAllEmployees extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO user = new UserDAO();
        try {
            ArrayList<User> display = user.getAllUsers();

            response.getWriter().append("<html><body><table><tr><th>user ID</th><th>username</th><th>password</th><th>first name</th><th>last name</th><th>email</th><th>user role ID</th></tr>");
            for(int i = 0; i < display.size(); i++)
            {
                response.getWriter().append("<td>" + display.get(i).getUserID() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getUsername() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getPassword() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getFirstName() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getLastName() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getEmail() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getUserRoleID() + "</td>");
                response.getWriter().append("</tr>" );
            }
            response.getWriter().append("</body></html></table>");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    }
}
