package com.example.ExpenseReimbursementSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ReimbursementDAO {

    public void updateReimbursement(Reimbursement reimbursement) throws SQLException {
        String query = "insert into TMReimbursement(" + "amount," + "description," + "user_ID," + "reimb_type," + "reimb_status," + ") VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ps.setDouble(2, reimbursement.getAmount());
        ps.setString(3, reimbursement.getDescription());
        ps.setInt(4, reimbursement.getUserID());
        ps.setString(5, reimbursement.getReimType());
        ps.setString(6, reimbursement.getReimbStatus());
        ps.executeQuery();
        ps.executeUpdate();
    }

    public ArrayList<Reimbursement> getSpecificRequest(int x) throws SQLException
    {
        String query = "select * from TMREIMBURSEMENT where UserID = x ";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        ArrayList<Reimbursement> r = new ArrayList<>();

        while(rs.next())
        {
            Reimbursement reimbursement = new Reimbursement();

            reimbursement.setReimbID(rs.getInt("REIMB_ID"));
            reimbursement.setAmount(rs.getDouble("AMOUNT"));
            reimbursement.setDescription(rs.getString("DESCRIPTION"));
            reimbursement.setReimType(rs.getString("REIMB_TYPE"));
            reimbursement.setUserID(rs.getInt("USER_ID"));
            reimbursement.setReimbStatus(rs.getString("REIMB_STATUS"));

            r.add(reimbursement);
        }

        for(int i = 0; i < r.size(); i++)
        {
            System.out.println(r.get(i));
        }

        return r;
    }

    public ArrayList<Reimbursement> getAllReimbursements() throws SQLException
    {
        String query = "select * from TMReimbursement";
        return getReimbursements(query);
    }

    public void viewReimbursements() throws SQLException
    {
        ArrayList<Reimbursement> viewUsers = getAllReimbursements();

        for(int i = 0; i < viewUsers.size(); i++)
        {
            System.out.println(viewUsers.get(i));
        }
    }

    public ArrayList<Reimbursement> viewPendingRequest() throws SQLException
    {
        String query = "select * from TMREIMBURSEMENT where REIMB_STATUS IS NULL";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();

        ArrayList<Reimbursement> r = new ArrayList<>();

        while(rs.next())
        {
            Reimbursement reimbursement = new Reimbursement();

            reimbursement.setReimbID(rs.getInt("REIMB_ID"));
            reimbursement.setAmount(rs.getDouble("AMOUNT"));
            reimbursement.setDescription(rs.getString("DESCRIPTION"));
            reimbursement.setReimType(rs.getString("REIMB_TYPE"));
            reimbursement.setUserID(rs.getInt("USER_ID"));
            reimbursement.setReimbStatus(rs.getString("REIMB_STATUS"));

            r.add(reimbursement);
        }

        for(int i = 0; i < r.size(); i++)
        {
            System.out.println(r.get(i));
        }

        return r;
    }



    public ArrayList<Reimbursement> viewResolvedRequest() throws SQLException
    {
        String query = "select * from TMReimbursement where REIMB_STATUS = resolved";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<Reimbursement> reimbursements = new ArrayList<>();
        while(rs.next())
        {
            Reimbursement reimbursement = new Reimbursement();
            reimbursement.setReimbID(rs.getInt("REIMB_ID"));
            reimbursement.setAmount(rs.getDouble("AMOUNT"));
            reimbursement.setDescription(rs.getString("DESCRIPTION"));
            reimbursement.setReimType(rs.getString("REIMB_TYPE"));
            reimbursement.setUserID(rs.getInt("USER_ID"));
            reimbursement.setReimbStatus(rs.getString("REIMB_STATUS"));

            reimbursements.add(reimbursement);
        }

        System.out.println("print results");

        for(int i = 0; i < reimbursements.size(); i++)
        {
            System.out.println(reimbursements.get(i));
        }

        return reimbursements;
    }

    public ArrayList<Reimbursement> viewApprovedRequest() throws SQLException
    {
        String query = "select * from TMReimbursement where REIMB_STATUS = approved";
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<Reimbursement> reimbursements = new ArrayList<>();
        while(rs.next())
        {
            Reimbursement reimbursement = new Reimbursement();
            reimbursement.setReimbID(rs.getInt("REIMB_ID"));
            reimbursement.setAmount(rs.getDouble("AMOUNT"));
            reimbursement.setDescription(rs.getString("DESCRIPTION"));
            reimbursement.setReimType(rs.getString("REIMB_TYPE"));
            reimbursement.setUserID(rs.getInt("USER_ID"));
            reimbursement.setReimbStatus(rs.getString("REIMB_STATUS"));

            reimbursements.add(reimbursement);
        }

        System.out.println("print results");

        for(int i = 0; i < reimbursements.size(); i++)
        {
            System.out.println(reimbursements.get(i));
        }

        return reimbursements;
    }

    public ArrayList<Reimbursement> getReimbursements(String query) throws SQLException {
        PreparedStatement ps = JDBC_Connection.getInstance().conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        ArrayList<Reimbursement> reimbursements = new ArrayList<>();
        while(rs.next())
        {
            Reimbursement reimbursement = new Reimbursement();
            reimbursement.setReimbID(rs.getInt("REIMB_ID"));
            reimbursement.setAmount(rs.getDouble("AMOUNT"));
            reimbursement.setDescription(rs.getString("DESCRIPTION"));
            reimbursement.setReimType(rs.getString("REIMB_TYPE"));
            reimbursement.setUserID(rs.getInt("USER_ID"));
            reimbursement.setReimbStatus(rs.getString("REIMB_STATUS"));

            reimbursements.add(reimbursement);
        }

        System.out.println("print results");

        for(int i = 0; i < reimbursements.size(); i++)
        {
            System.out.println(reimbursements.get(i));
        }

        return reimbursements;
    }

}
