package com.example.ExpenseReimbursementSystem;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "ViewApproved", value = "/ViewApproved")
public class ViewApproved extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ReimbursementDAO reimbursementDAO = new ReimbursementDAO();

        try {
            ArrayList<Reimbursement> display = reimbursementDAO.viewApprovedRequest();

            response.getWriter().append("<html><body><table><tr><th>ReimbID</th><th>Amount</th><th>Description</th><th>Reim Type</th><th>Reib Status</th><th>User ID</th></tr>");
            for (int i = 0; i < display.size(); i++) {
                response.getWriter().append("<td>" + display.get(i).getReimbID() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getAmount() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getDescription() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getReimType() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getReimbStatus() + "</td>");
                response.getWriter().append("<td>" + display.get(i).getUserID() + "</td>");
                response.getWriter().append("</tr>");
            }
            response.getWriter().append("</body></html></table>");

        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
