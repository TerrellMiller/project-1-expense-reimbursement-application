package com.example.ExpenseReimbursementSystem;

public class UserRole {
    private int userRoleID;
    private String userRole;

    public UserRole()
    {}

    public UserRole(int userRoleID, String userRole) {
        this.userRoleID = userRoleID;
        this.userRole = userRole;
    }

    public int getUserRoleID() {
        return userRoleID;
    }

    public void setUserRoleID(int userRoleID) {
        this.userRoleID = userRoleID;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
